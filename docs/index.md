# Residual 네트워크와 ResNet 아키텍처 <sup>[1](#footnote_1)</sup>

> <font size="3"Residual 네트워크와 ResNet 아키텍처가 딥러닝에 어떻게 사용되는지 알아본다.</font>

## 목차

1. [개요](./residual-networks-and-resnet-architecture.md#intro)
1. [Residual 네트워크란?](./residual-networks-and-resnet-architecture.md#sec_02)
1. [Residual 네트워크가 효과적인 이유](./residual-networks-and-resnet-architecture.md#sec_03)
1. [Python에서 Residual 네트워크 구현](./residual-networks-and-resnet-architecture.md#sec_04)
1. [ResNet 아키텍처란?](./residual-networks-and-resnet-architecture.md#sec_05)
1. [이미지 분류를 위한 ResNet 사용법](./residual-networks-and-resnet-architecture.md#sec_06)
1. [요약](./residual-networks-and-resnet-architecture.md#summary)

<a name="footnote_1">1</a>: [DL Tutorial 8 — Residual Networks and ResNet Architecture](https://ai.plainenglish.io/dl-tutorial-8-residual-networks-and-resnet-architecture-ca7e2e3cc26d?sk=6fcf500c0a424b25ad30c1568a9a9139)를 편역하였다.
